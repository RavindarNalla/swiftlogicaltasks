import Foundation
import UIKit

// Snake Pattern
func snakePattern(num:Int) {
    var isReversed:Bool = false
    for j in 1...num {
        if j%2 == 0 {
            isReversed = true
        }
        if isReversed {
            for m in (0..<num) {
                print(j*num-m, terminator : " ")
            }
            isReversed = false
        } else if j > 1 {
            for k in (0..<num).reversed() {
                print(j*num-k, terminator : " ")
            }
            isReversed = true
        }else {
            for i in 1...num {
                print(i, terminator : " ")
            }
        }
        print(" ")
    }
}
snakePattern(num: 6)

//1. Given an array of size N, it contains all the numbers from 1 to N+1 inclusive, except one number. You have to find the missing number.
var arrOfNumbers:[Int] = [3, 5, 8, 1, 4, 7, 2]
func missingNumber(N:Int, arr:[Int]) {
    var sum = 0
    var sum2 = 0
    for i in 0..<N+1 {
        sum = sum + i+1
        if arr.count > i {
            sum2 = sum2 + arr[i]
        }
    }
    print(sum-sum2)
}
missingNumber(N: 7, arr: arrOfNumbers)

func missing(N:Int,arr:[Int]) {
    var missing:Int = (N * (N+1))/2
    for i in 0..<N+1 {
        if i < N-1 {
            missing -= arr[i]
        }
    }
    print(missing)
}
missing(N:8, arr: arrOfNumbers)


//2. Count the number of trailing 0s in factorial of a given number.
func findNumOfZeros(n:Int) {
    var numOfZeros = 0
    var countNum = 5
    while n / countNum >= 1 {
        numOfZeros += n / countNum
        countNum *= 5
    }
    print(numOfZeros)
}

findNumOfZeros(n: 5)


//1.find index of a char or substring in a string
 func indexOfCharOrSubStr(inStr: String, charOrSubStr:String) -> Int {
    if let range = inStr.range(of: charOrSubStr) {
        let index = inStr.distance(from: charOrSubStr.startIndex, to: range.lowerBound)
        return (index)
    } else {
        return 0
    }
}
indexOfCharOrSubStr(inStr: "jackpot", charOrSubStr:"t")
func findIndex(OfStr1:String, InStr2:String) {
    if let range = InStr2.range(of: OfStr1) {
      let substring = InStr2[..<range.lowerBound] // or str[str.startIndex..<range.lowerBound]
      print(substring)
    }
    else {
      print("String not present")
    }
}
findIndex(OfStr1:"pot", InStr2:"jackpot")


//2.find most appeared char in string
func mostAppearedChar(str:String) {
    let uniqueChar = Set(str)
    var dic:[Character:Int] = [:]
    for ch in uniqueChar {
        let charCount = str.filter{$0 == ch}.count
        dic[ch] = charCount
    }
    let highestNum = dic.filter {$0.value == dic.values.max()}
    print(highestNum)
}
mostAppearedChar(str: "Nalllaall")
func mostAppeared(str:String) {
    var mainStr = str
    var dic:[Character:Int] = [:]
    for ch in mainStr {
        let count = str.filter{$0 == ch}.count
        dic[ch] = count
        mainStr = mainStr.replacingOccurrences(of: String(ch), with: "")
    }
    let highestNum = dic.filter {$0.value == dic.values.max()}
    print(highestNum)
}
var str:String = "Nllkkk"
mostAppeared(str: str)


//3.generic func example
protocol Addable {
    static func +(lhs: Self, rhs: Self) -> Self
}

extension Double : Addable {}
extension Float  : Addable {}
extension Int    : Addable {}
extension String : Addable {}

func addGenerically<T:Addable>(lhs:T, rhs:T) -> T {
    return lhs + rhs
}
addGenerically(lhs: "sdnbfsjd", rhs: "sdnbsm")


//4. anagram
func isAnagram(str1:String, str2:inout String) -> Bool {
    var arr:[Character] = []
    var str3:String = ""
    for char in str2 {
        arr.insert(char, at: arr.startIndex)
        str3 = String(char) + str3
    }
    str2 = String(arr)
    return str1 == str3
}
var str12 = "ravindar"
isAnagram(str1: "radnivar", str2: &str12)

func isArrayAnagram(string1:[String],string2:[String]) {
    var arr:[Int] = []
    for i in 0..<string1.count {
        if string1[i] == String(string2[i].reversed()) {
            arr.append(1)
        } else {
            arr.append(0)
        }
    }
}
isArrayAnagram(string1: ["tea", "tea", "act"], string2: ["aet", "toa", "taes"])


//5.flatten array
var arrTemp:[Any] = []
var flag:Bool = true
func flattenArray(arr:[Any]) {
    for item in arr {
        if item is [Any] {
            flattenArray(arr: item as! [Any])
            flag = false
        } else {
            arrTemp.append(item)
            flag = true
        }
    }
    /*for i in 0..<arr.count {

        if arr[i] is [Any] {
            flattenArray(arr: arr[i] as! [Any])
        } else {
            arrTemp.append(arr[i] as! Int)
        }
    }*/
    if flag {
        print(arrTemp)
    }
}
flattenArray(arr: [3, 4, [4, [ [55] ] ] ])


//6. denomination
func currencyCount(amt:inout Int) {
    let arrOfTotalNotes = [2000, 500, 200, 100, 50, 20, 10]
    var dicOfActualNotes:[Int:Int] = [:]
    for i in 0..<arrOfTotalNotes.count {
        if amt >= arrOfTotalNotes[i] {
            dicOfActualNotes[arrOfTotalNotes[i]] = amt/arrOfTotalNotes[i]
            amt = amt%arrOfTotalNotes[i]
        }
    }
    print(dicOfActualNotes)
    
}
var amt:Int = 2890
currencyCount(amt: &amt)


//7. factorial
var arr = ["even","odd"]
print(arr[7%2], terminator: "")

func factorial(num:Int) -> Int {

   if (num == 0) {
       return 1
   }
   let sum = num * factorial(num:num - 1 )

   return sum
}
factorial(num: 4)


//8.indices
var numArray2:[Int] = [1,2,5,9]
var sum = 7

func indicesForSum(sum: Int, inArr arr: [Int]) -> [Int] {
    let arrTemp = arr
    var indicesOne:Int = 0
    var indicesTwo:Int = 0

    for i in 0..<arr.count {
        for j in 0..<arrTemp.count {
            if j != i {
                if arr[i] + arrTemp[j] == sum {
                    indicesOne = i
                    indicesTwo = j
                    break
                }
            }
        }
    }
    return [indicesTwo, indicesOne]
}
indicesForSum(sum: 14, inArr: numArray2)


//9. highest value
func someMethod(inArr arr:[Int]) {
    var highestSum:Int = 0
    var arrNew:[Int] = []
    
    for i in 0..<arr.count {
        for j in 0..<arr.count {
            if j != i {
                highestSum = arr[i] * arr[j]
                arrNew.append(highestSum)
            }
        }
    }
    arrNew.sort()
    print(arrNew.last!)
}
someMethod(inArr: numArray2)

let arr3 = [1, 2, 3, 4, 5]
print(arr3.reduce(0,+))

extension Collection where Element : Numeric {
    var total:Element {reduce(0,+) }
}
arr3.total

let Scores = ["Rohit":[20,30,50], "Shubham" : [10,15,20], "Pujara" : [22,25,37],"Virat" : [50,60, 70]]
var total = Scores.flatMap{$0.value}
print(total)


/*
final class singleton {
     private static let sharedInstanceOfSingleton = singleton()
}
var arr:[Int] = [1,5,2,3,4,4,3]
func checkForDuplicates(arr:[Int]) -> Bool {
    let uniqueSet =  Set(arr)
    if arr.count != uniqueSet.count {
        return false
    } else {
        return true
    }
}
 checkForDuplicates(arr: arr)

let data = ["john": 23, "james": 24, "vincent": 34, "louis": 29]
var arrOfNames = data.map { $0.key }.sorted()
var arrOfAges = data.map { $0.value }.sorted()
print(arrOfNames, arrOfAges)

func modifyParams(a: inout Int) {
    a = 20
    print(a)
}
var num = 4
modifyParams(a: &num)
*/


/*
 var number = 42
 
 number.isMultiple(of: 5)
 
 var arrOfNumbers = [1, 5, 3, 8, 9]

 var arr1 = arrOfNumbers.sorted()
*/


 /*
 let serialQueue = DispatchQueue(label: "queuename")
 serialQueue.sync {

 }
 
 let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
 concurrentQueue.sync {

 }
 
 DispatchQueue.main.async {

 }
 
 DispatchQueue.main.sync {

 }
 
 DispatchQueue.global(qos: .background).async {

 }
 DispatchQueue.global(qos: .default).async {

 }
 
 DispatchQueue.global().async {
     // qos' default value is ´DispatchQoS.QoSClass.default`
 }
 
 let queue = OperationQueue()
 queue.maxConcurrentOperationCount = 4  // a max of 4 decompress tasks at a time

 let completion = BlockOperation {
     // all done
 }

 /*for file in compressedFiles {
     let operation = BlockOperation {
         file.decompress()
     }
     completion.addDependency(operation)
     queue.addOperation(operation)
 } */

 OperationQueue.main.addOperation(completion)
 
 var sample1 = {
     print("Hello")
 }
 sample1()

 let sample2 = { (name:String) in
     print(name)
 }
 sample2("Hii")

 var findSqr = { (num:Int) -> Int in
     let sqr = num*num
     return sqr
 }
 findSqr(4)

 func grabLunch(search: () -> ()) {
   // closure call
   search()
 }
 
var handler = {(_ Str:String) in
    
    print("Text")
}

print(handler)

class singletonObject {
    private static let sample = singletonObject()
}
*/


/*
let group = DispatchGroup()
let serialQueue: DispatchQueue = DispatchQueue(label: "someName")

dispatch_sync(serialQueue, ^{
    //block1
})

let group = DispatchGroup()
group.enter()
//perform task
group.leave()
// task1 task2 task4
*/


/*let arrSec = [["Sachin": 45, "Dravid": 43, "Ganguly": 46, "Dhoni": 40, "Rohit": 38, "Kohli": 38]]

var dicTempSec:[String:Int] = arrSec[0]

let newArrNames = dicTempSec.map { $0.key }.sorted()
let newArrScores = dicTempSec.map { $0.value }.sorted()
print(newArrNames, newArrScores)*/


//let arr = ["batt", "apple", "zebra", "plane", "even"]
//let result = arr.map {$0.count}.filter{$0 % 2 == 0}
//print(result)

/*
let arr2 = [1, 2, 3, 4, 5]

func check(arr:[Int]) {
    
    for item in arr {
        
        switch item % 2 == 0 {
        case true:
            print("Hi")
        default:
            print("Hello")
        }
    }
        
}
check(arr: arr2)

 
func compressString(str:String) -> String {
    var count:Int = 0
    for char in str {
        
        str.forEach { char1 in
            if char1 == char {
                count += 1
            }
        }
        
    }
    return String("\(count)")
}
compressString(str: "nbcd")


struct Person {
     let name: String

    init(name: String)
    {
        self.name = name
    }
}

let per = Person(name: "myName")


var greeting = "Hello, playground"

class Employee {
    var name: String?
    var age: Int? // Years
    var salary: Double?
    init(name: String, age: Int, salary: Double) {
        self.name = name
        self.age = age
        self.salary = salary
    }
    deinit {
        print("Releasing \(name ?? "")")
    }
}

var emp: Employee? = Employee(name: "Abcd", age: 35, salary: 10000)

var emp2 = emp

emp = nil

let op1: Int = 1
let op2: UInt = 2
let op3: Double = 3.34
var result = Double(op1) + Double(op2) + op3
*/


/*
let age: String? = nil
func checkAge(age1:String?) {
  guard let myAge = age1 else {
    print("Age is undefined")
    return
  }
  print("My age is \(myAge)")
}
checkAge(age1: age)

let isTrue: Bool? = false
func checkFortrue(isTr:Bool?) {
    guard isTr ?? true else {
    print("False")
    return
  }
  print("True")
}
checkFortrue(isTr: isTrue)

guard true else {
    print("False")
}
var name =  "Ravindar" * 99
func * (LHS:String, RHS:Int) {
    
}
*/

/*
func consecutive(n:Int) -> Int {
    var i = 1, j = 1, count = 0, sum = 1
        while (j<n) {
            if (sum == n) { // matched, move sub-array section forward by 1
                count += 1
                sum -= i
                i += 1
                j += 1
                sum += j
            } else if (sum < n) { // not matched yet, extend sub-array at end
                j += 1
                sum += j
            } else { // exceeded, reduce sub-array at start
                sum -= i
                i += 1
            }
        }
        return count
}
consecutive(n: 15)
*/

/*
func isAnagram(string1: [String], string2: [String]) -> [Int] {

    var arr:[Int] = []
    for fIndex in 0..<string1.count {
        let strTemp1 = string1[fIndex]
        let strTemp2 = string2[fIndex]
        
        if countedSet(string: strTemp1).isEqual(countedSet(string: strTemp2)) {

            arr.append(0)
           
        }else{
            if strTemp1.count != strTemp2.count {
                arr.append(-1)
            }else{
                arr.append(1)
            }
        }
    }
return arr
}
func countedSet(string: String) -> NSCountedSet {
    let array = string.map { (character) -> String in
        return String(character)
    }
    print(array)
    return NSCountedSet(array: array)
}

isAnagram(string1: ["tea", "tea", "act"], string2: ["ate", "toa", "taes"])
*/
